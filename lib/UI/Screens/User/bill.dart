import 'package:flutter/material.dart';

class BillScreen extends StatelessWidget {
  const BillScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Bill details'),
      ),
      body: ListView(
        children: [
          Container(
            width: double.infinity,
            height: size.height*.25,
            margin: EdgeInsets.symmetric(vertical: size.width*0.05),
            decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('images/bill.png'),
              ),
            ),
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.home_filled),
            title: Text('Hospital Name'),
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.monetization_on_outlined),
            title: Text('Total : 500 \$'),
          ),
          ListTile(
            leading: Icon(Icons.task_outlined),
            title: Text('Medical insurance : 60% ( 300 \$ )'),
          ),
          ListTile(
            leading: Icon(Icons.payments_outlined),
            title: Text('Final bill : 40% ( 200 \$ )'),
          ),
          ListTile(
            leading: Icon(Icons.error_outline,color: Colors.red,),
            title: Text('not payed yet!'),
          ),
          ListTile(
            leading: Icon(Icons.check,color: Colors.green,),
            title: Text('payed'),
          ),
        ],
      ),
    );
  }
}
