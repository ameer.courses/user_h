
class DepartmentModel {
  DepartmentModel({
    required this.id,
    required this.hospitalId,
    required this.name,
    required this.createdAt,
    required this.updatedAt,
  });

  final int id;
  final int hospitalId;
  final String name;
  final DateTime createdAt;
  final DateTime updatedAt;

  factory DepartmentModel.fromJson(Map<String, dynamic> json) => DepartmentModel(
    id: json["id"],
    hospitalId: json["hospital_id"],
    name: json["name"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

}
