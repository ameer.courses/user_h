import 'package:user/Logic/API/Controllers/AuthController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:user/UI/Widgets/inputs.dart';

import '../../../constants.dart';

class LoginScreen extends StatelessWidget {


  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: size.height*.25,
              padding: EdgeInsets.symmetric(vertical: size.width*0.025),
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('images/login.jpg'),
                    fit: BoxFit.cover
                ),
              ),
            ),
            MyTextField(
              hint: 'email',
              icon: Icon(Icons.email_outlined),
              keyboardType: TextInputType.emailAddress,
              controller: emailController,
            ),
            MyTextField(
              hint: 'password',
              icon: Icon(Icons.password_outlined),
              isPass: true,
              controller: passController,
            ),
            SizedBox(
              height: size.height*0.05,
            ),
            ElevatedButton(
                onPressed: ()async{
                  if(emailController.text.isEmpty || passController.text.isEmpty ){
                    snack(context, 'missing data');
                    return;
                  }
                  showLoading(context);
                  try{
                    var res = await AuthController.login(
                        email: emailController.text,
                        password: passController.text);
                    snack(context, res);
                    Get.offNamed('/home');
                  }
                  catch(e){
                    snack(context, e.toString());
                    Navigator.pop(context);
                  }
                },
                child: Text('Login')
            ),
            SizedBox(
              height: size.height*0.1,
            ),
          ],
        ),
      ),
    );
  }
}
