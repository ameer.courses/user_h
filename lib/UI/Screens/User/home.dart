// ignore_for_file: use_key_in_widget_constructors, prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../Logic/API/Controllers/HospitalController.dart';
import '../../../Logic/API/Models/HospitalModel.dart';
import '../../Widgets/Views/bill.dart';
import '../../Widgets/Views/hospital.dart';


class Home extends StatefulWidget {
  static final List<String> names = [
    'Hospitals',
    'Emergency call',
    'Bills',
    'Medical insurance'
  ];

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final PageController pController = PageController();

  int _inx = 0;


  @override
  void initState() {
    pController.addListener(() =>setState((){
      _inx = pController.page?.round() ?? 0;

    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Home.names[_inx] ),

      ),
      body: PageView(
        controller: pController,
        physics: NeverScrollableScrollPhysics(),
        children: [
          FutureBuilder<List<HospitalModel>>(
              future: HospitalController.myHospitals(),
              builder: (context,snapShot) {
                if(snapShot.hasData){
                  return ListView.builder(
                      itemCount: snapShot.data!.length,
                      itemBuilder: (context, i) => HospitalView(snapShot.data![i])
                  );
                }
                return Center(
                  child: snapShot.hasError ? Text(snapShot.error.toString())
                      : CircularProgressIndicator(),
                );
              }
          ),
          EmergencyPage(),
          ListView.builder(
              itemCount: 10,
              itemBuilder: (context,i) =>
                  BillView()
          ),
          Container(color: Colors.teal,),
        ],
      ),
      bottomNavigationBar: BottomNavyBar(

        animationDuration: Duration(milliseconds: 40),
        selectedIndex: _inx,
        onItemSelected: (inx) async{
              pController.jumpToPage(inx);
        },
        items: [
          BottomNavyBarItem(
              icon: Icon(Icons.home_filled), title: Text('Hospitals'),
              activeColor: Colors.blueGrey
          ),
          BottomNavyBarItem(
              icon: Icon(Icons.local_shipping), title: Text('Emergency'),
              activeColor: Colors.redAccent
          ),
          BottomNavyBarItem(
              icon: Icon(Icons.insert_drive_file), title: Text('Bills'),
              activeColor: Colors.green
          ),
          BottomNavyBarItem(
              icon: Icon(Icons.task_outlined), title: Text('Insurance'),
              activeColor: Colors.teal
          ),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
              child: Container(

              )
            ),
            ListTile(
              leading: Icon(Icons.local_shipping_outlined),
              title: Text('Emergency'),
              trailing: Icon(Icons.phone_in_talk),
              onTap: ()async => await launchUrl(Uri.parse('tel:911')),
            ),
            ListTile(
              leading: Icon(Icons.home_filled),
              title: Text('Hospitals'),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: () {
                pController.jumpToPage(0);
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.insert_drive_file),
              title: Text('Bills'),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: () {
                pController.jumpToPage(2);
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.task_outlined),
              title: Text('Medical insurance'),
              trailing: Icon(Icons.arrow_forward_ios),
              onTap: () {
                pController.jumpToPage(3);
                Navigator.pop(context);
              },
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.info_outline),
              title: Text('info'),
              trailing: Icon(Icons.question_mark_sharp),
            ),
            ListTile(
              leading: Icon(Icons.logout),
              title: Text('logout'),
            ),

          ],
        ),
      ),
    );
  }
}

class EmergencyPage extends StatelessWidget {
  const EmergencyPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(Get.width*0.05),
            child: Image.asset('images/amb.png'),
          ),
          SizedBox(
            height: Get.height*0.2,
          ),
          ElevatedButton.icon(
              onPressed: ()async{
                await launchUrl(Uri.parse('tel:911'));
              },
              icon: Icon(Icons.phone_in_talk),
              label: Text('Call now'),
              style: ElevatedButton.styleFrom(
                primary: Colors.redAccent
              ),
          )
        ],
      ),
    );
  }
}

