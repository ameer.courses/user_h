// ignore_for_file: unnecessary_string_interpolations, curly_braces_in_flow_control_structures

import 'package:user/Logic/API/Models/UserModel.dart';
import 'package:get_storage/get_storage.dart';
import 'package:user/constants.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class AuthController{


  static Future<String> login({required String email, required String password,})async{
    var response = await http.post(
        Uri.parse('$authUrl/login'),
        body: jsonEncode({
          'email':email,
          'password':password,
        }),
        headers: {
          'Content-Type': 'application/json'
        }
    );
    Map<String,dynamic> json = jsonDecode(response.body);

    if(json['token'] != null){
      await GetStorage().write('token', json['token']);
      return 'Login successfully , hello ${json['user']['name']}';
    }
    throw Exception(json['error']);
  }

  static Future<String> logout()async{
    var response = await http.delete(
        Uri.parse('$url/logout'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ${GetStorage().read('token')}',
          'Accept' : 'application/json',
        }
    );
    Map<String,dynamic> json = jsonDecode(response.body);

    await GetStorage().remove('token');

    return json['message'];
  }


}