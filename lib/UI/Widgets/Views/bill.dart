import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../constants.dart';
import '../../Screens/User/bill.dart';

class BillView extends StatelessWidget {
  const BillView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () => Get.to(BillScreen()),
      child: Container(
        height: size.width*0.5,
        width: size.width*0.75,
        margin: EdgeInsets.all(size.width*0.025),
        padding: EdgeInsets.all(size.width*0.02),
        decoration: BoxDecoration(
            color: customGray,
            borderRadius: BorderRadius.circular(size.width*0.05)
        ),
        child: Row(
          children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.all(size.width*0.035),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(size.width*0.05),
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage('images/bill.png')
                    )
                ),

              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(size.width*0.025),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Bill',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16
                      ),
                    ),
                    SizedBox(height: size.height*0.005,),
                    Row(
                      children: [
                        Icon(Icons.date_range_outlined,size: size.width*0.05,),
                        Text('2022-9-17')
                      ],
                    ),
                    SizedBox(height: size.height*0.005,),
                    Row(
                      children: [
                        Icon(Icons.home_filled,size: size.width*0.05,),
                        Text('Hospital name')
                      ],
                    ),
                    SizedBox(height: size.height*0.01,),
                    Row(
                      children: [
                        Icon(Icons.monetization_on_outlined,size: size.width*0.05,),
                        Text('140 \$')
                      ],
                    ),
                    SizedBox(height: size.height*0.01,),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
