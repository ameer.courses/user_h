import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:user/Logic/API/Models/HospitalModel.dart';
import '../../../constants.dart';
import '../../Screens/User/hospital.dart';

class HospitalView extends StatelessWidget {

  final HospitalModel model;


  HospitalView(this.model);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () => Get.to(HospitalScreen(model)),
      child: Container(
        height: size.width*0.5,
        width: size.width*0.75,
        margin: EdgeInsets.all(size.width*0.025),
        padding: EdgeInsets.all(size.width*0.02),
        decoration: BoxDecoration(
            color: customGray,
            borderRadius: BorderRadius.circular(size.width*0.05)
        ),
        child: Row(
          children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(size.width*0.05),
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage('images/hospital.png')
                    )
                ),

              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(size.width*0.025),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      model.name,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16
                      ),
                    ),
                    SizedBox(height: size.height*0.005,),
                    Row(
                      children: [
                        Icon(Icons.location_city_outlined,size: size.width*0.05,),
                        Text(model.address)
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
