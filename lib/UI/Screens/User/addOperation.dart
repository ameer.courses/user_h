import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:user/UI/Widgets/inputs.dart';

import '../../../Logic/API/Controllers/HospitalController.dart';
import '../../../constants.dart';

class AddOperationScreen extends StatelessWidget {

  final int department_id;
  final int hospital_id;
  final TextEditingController descriptionController = TextEditingController();

  AddOperationScreen(this.department_id, this.hospital_id);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Operation'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: Get.height*0.05,),
            MyTextField(
              maxL: 4,
              hint: 'description',
              controller: descriptionController,
            ),
            SizedBox(height: Get.height*0.075,),
            ElevatedButton(
                onPressed: ()async{
                  if(descriptionController.text.isEmpty)
                    return;
                  showLoading(context);
                  await HospitalController.addOperation(
                      hospital_id: hospital_id,
                      department_id: department_id,
                      description: descriptionController.text
                  );
                  Get.offAllNamed('/home');
                },
                child: Text('Submit')
            )
          ],
        ),
      ),
    );
  }
}