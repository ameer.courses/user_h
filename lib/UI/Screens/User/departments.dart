import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:user/Logic/API/Controllers/HospitalController.dart';
import 'package:user/Logic/API/Models/HospitalModel.dart';
import 'package:user/UI/Screens/User/addOperation.dart';
import 'package:user/UI/Widgets/inputs.dart';
import 'package:user/constants.dart';

import '../../../Logic/API/Models/DepartmentModel.dart';



class DepartmentsScreen extends StatelessWidget {


  final HospitalModel model;


  DepartmentsScreen(this.model);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Departments'),
      ),
      body: FutureBuilder<List<DepartmentModel>>(
        future: HospitalController.departments(model.id),
        builder: (context,snapShot){
          if(snapShot.hasData){
            return ListView.builder(
              itemCount: snapShot.data!.length,
                itemBuilder: (context,i)=>
                    ListTile(
                      leading: Icon(Icons.location_city_outlined),
                      title: Text(snapShot.data![i].name),
                      trailing: Icon(Icons.arrow_forward_ios),
                      onTap: ()=> Get.to(AddOperationScreen(snapShot.data![i].id, model.id)),
                    )
            );
          }
          return Center(
            child: snapShot.hasError ? Text(snapShot.error.toString())
                : CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
