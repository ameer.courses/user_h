import 'package:flutter/material.dart';
const customGray = Color.fromRGBO(223, 223, 223, 1);
const url = 'http://192.168.137.1:8000/api/user';
const authUrl = 'http://192.168.137.1:8000/api/user';

void showLoading(context) => showDialog(
    context: context,
    builder: (context)=>
        WillPopScope(
          onWillPop: ()async{
            return false;
          },
          child: Center(
            child: CircularProgressIndicator(),
          ),
        )
);

void snack(context,text) =>
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(text)));