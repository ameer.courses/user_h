
class RequestModel {
  RequestModel({
    required this.name,
    required this.id,
    required this.hospitalId,
    required this.companyId,
    required this.accept,
    required this.createdAt,
    required this.updatedAt,
    required this.isCompany,
    required this.address
  });

  final String name;
  final String address;
  final int id;
  final int hospitalId;
  final int companyId;
  final dynamic accept;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int isCompany;

  factory RequestModel.fromJson(Map<String, dynamic> json) => RequestModel(
    name: json["name"],
    id: json["id"],
    hospitalId: json["hospital_id"],
    companyId: json["company_id"],
    accept: json["accept"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    isCompany: json["isCompany"],
    address: json['address']
  );


}
