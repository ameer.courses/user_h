
class UserModel {
  UserModel({
    required this.user,
    required this.password,
    required this.patient,
  });

  final User user;
  final String password;
  final Patient patient;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
    user: User.fromJson(json["user"]),
    password: json["password"],
    patient: Patient.fromJson(json["patient"]),
  );

  Map<String, dynamic> toJson() => {
    "user": user.toJson(),
    "password": password,
    "patient": patient.toJson(),
  };
}

class Patient {
  Patient({
    required this.userId,
    required this.companyId,
    required this.ratio,
    required this.updatedAt,
    required this.createdAt,
    required this.id,
  });

  final int userId;
  final int companyId;
  final int ratio;
  final DateTime updatedAt;
  final DateTime createdAt;
  final int id;

  factory Patient.fromJson(Map<String, dynamic> json) => Patient(
    userId: json["user_id"],
    companyId: json["company_id"],
    ratio: json["ratio"],
    updatedAt: DateTime.parse(json["updated_at"]),
    createdAt: DateTime.parse(json["created_at"]),
    id: json["id"],
  );

  Map<String, dynamic> toJson() => {
    "user_id": userId,
    "company_id": companyId,
    "ratio": ratio,
    "updated_at": updatedAt.toIso8601String(),
    "created_at": createdAt.toIso8601String(),
    "id": id,
  };
}

class User {
  User({
    required this.name,
    required this.email,
    required this.phone,
    required this.role,
    required this.address,
    required this.isVerified,
    required this.updatedAt,
    required this.createdAt,
    required this.id,
  });

  final String name;
  final String email;
  final String phone;
  final String role;
  final String address;
  final bool isVerified;
  final DateTime updatedAt;
  final DateTime createdAt;
  final int id;

  factory User.fromJson(Map<String, dynamic> json) => User(
    name: json["name"],
    email: json["email"],
    phone: json["phone"],
    role: json["role"],
    address: json["address"],
    isVerified: json["is_verified"],
    updatedAt: DateTime.parse(json["updated_at"]),
    createdAt: DateTime.parse(json["created_at"]),
    id: json["id"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "email": email,
    "phone": phone,
    "role": role,
    "address": address,
    "is_verified": isVerified,
    "updated_at": updatedAt.toIso8601String(),
    "created_at": createdAt.toIso8601String(),
    "id": id,
  };
}
