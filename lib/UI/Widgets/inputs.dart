import 'package:flutter/material.dart';

class MyTextField extends StatefulWidget {

  final TextEditingController? controller;
  final Widget? icon;
  final String? hint;
  final TextInputType? keyboardType;
  final bool isPass ;
  final int maxL;
  bool _hide = true;
  MyTextField({this.controller, this.icon, this.hint,this.keyboardType,this.isPass = false,this.maxL = 1});

  @override
  State<MyTextField> createState() => _MyTextFieldState();
}

class _MyTextFieldState extends State<MyTextField> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.all(size.width*0.025),
      child:
      TextField(
        maxLines: widget.maxL,
        controller: widget.controller,
        obscureText: widget.isPass && widget._hide,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(size.width*0.025)
          ),
          filled: true,
          fillColor: Colors.grey.withOpacity(0.35),
          hintText: widget.hint,
          prefixIcon: widget.icon,
          suffixIcon: widget.isPass? IconButton(
            icon: Icon(
                widget._hide ? Icons.visibility_outlined :
                Icons.visibility_off_outlined
            ),
            onPressed: (){
              setState(() {
                widget._hide = !widget._hide;
              });
            },
          ):null
        ),
        keyboardType: widget.keyboardType,
      ),
    );
  }
}
