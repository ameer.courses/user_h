class HospitalModel {
  HospitalModel({
    required this.id,
    required this.name,
    required this.email,
    required this.phone,
    required this.role,
    required this.address,
    required this.isVerified,

    required this.createdAt,
    required this.updatedAt,
    required this.status,
  });

  final int id;
  final String name;
  final String email;
  final String phone;
  final String role;
  final String address;
  final int isVerified;

  final DateTime createdAt;
  final DateTime updatedAt;
  final bool status;

  factory HospitalModel.fromJson(Map<String, dynamic> json) =>
      HospitalModel(
        id: json["id"],
        name: json["name"],
        email: json["email"],
        phone: json["phone"],
        role: json["role"],
        address: json["address"],
        isVerified: json["is_verified"],

        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        status: json["status"] ?? false,
      );
}