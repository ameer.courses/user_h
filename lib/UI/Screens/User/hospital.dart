import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:user/Logic/API/Models/HospitalModel.dart';
import 'package:user/UI/Screens/User/departments.dart';

class HospitalScreen extends StatelessWidget {


  final HospitalModel model;


  HospitalScreen(this.model);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(model.name),
      ),
      body: ListView(
        children: [
          Container(
            width: double.infinity,
            height: size.height*.25,
            margin: EdgeInsets.symmetric(vertical: size.width*0.025),
            decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('images/doctors.jpg'),
                  fit: BoxFit.cover
              ),
            ),
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.location_on_outlined),
            title: Text(model.address),
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.local_shipping_outlined),
            title: Text('Emergency call'),
            trailing: Icon(Icons.phone_in_talk),
            onTap: ()async =>
              await launchUrl(Uri.parse('tel:911'))
          ),
          ListTile(
            leading: Icon(Icons.medical_information_outlined),
            title: Text('Departments'),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: ()=> Get.to(DepartmentsScreen(model)),
          ),

        ],
      ),
    );
  }
}