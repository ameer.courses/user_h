// ignore_for_file: unnecessary_string_interpolations, curly_braces_in_flow_control_structures

import 'package:user/Logic/API/Models/HospitalModel.dart';
import 'package:user/Logic/API/Models/RequestModel.dart';
import 'package:get_storage/get_storage.dart';
import 'package:user/constants.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

import '../Models/DepartmentModel.dart';

class HospitalController{


  static Future<List<HospitalModel>> myHospitals() async {
    var response = await http.get(
        Uri.parse('$url/hospitals'),
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ${GetStorage().read('token')}'
        }
    );
    Map<String,dynamic> json = jsonDecode(response.body);
    print(json);
    List<HospitalModel> hospitals = [];

    if(json['message']!=null)
      return [];
    for( var h in json['hospitals'])
      hospitals.add(HospitalModel.fromJson(h));

    print(hospitals);
    return hospitals;
  }

  static Future<List<DepartmentModel>> departments(id) async {
    var response = await http.get(
        Uri.parse('$url/hospital/departments/$id'),
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ${GetStorage().read('token')}'
        }
    );
    Map<String,dynamic> json = jsonDecode(response.body);
    print(json);
    List<DepartmentModel> departments = [];

    if(json['departments']==null)
      return [];
    for( var h in json['departments'])
      departments.add(DepartmentModel.fromJson(h));

    print(departments);
    return departments;
  }


  static Future<void> addOperation({
  required int hospital_id,
    required int department_id,
    required String description,
}) async {
    var response = await http.post(
        Uri.parse('$url/operations/add'),
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ${GetStorage().read('token')}',
          'Content-Type' : 'application/json',
        },
      body: jsonEncode({
        'hospital_id':hospital_id,
        'department_id':department_id,
        'description' :description
      })
    );

    print(response.body);
  }


}