import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'UI/Screens/Auth/login.dart';
import 'package:get/get.dart';



import 'UI/Screens/User/addOperation.dart';
import 'UI/Screens/User/home.dart';

void main() async {
  await GetStorage.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
        iconTheme: IconThemeData(
          color: Color.fromRGBO(35, 79, 104, 1)
        ),
        appBarTheme: AppBarTheme(
          centerTitle: true,
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(AppBar().preferredSize.height*0.25)
            )
          )
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            elevation: 0
          )
        ),
        listTileTheme: ListTileThemeData(
          iconColor: Colors.blueGrey[700]
        )
      ),
      getPages: [
        GetPage(name: '/home', page: ()=> Home()),

      ],
      home: GetStorage().hasData('token') ? Home() :  LoginScreen(),
    );
  }
}
